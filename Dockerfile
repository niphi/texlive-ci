FROM alpine:latest

# OpenContainers Label Specification:
# https://github.com/opencontainers/image-spec/blob/main/annotations.md
LABEL org.opencontainers.image.title="TeXLive CI Container"
LABEL org.opencontainers.image.description="Docker Image for CI Servers featuring TeXLive, currently assembled by running the TeXLive netinstaller in an alpine:latest image"
# authors, urls and so forth are probably better set at build time

# tl-install requirements:
# - perl. Permenently needed.
# - a 'real' wget.
# - gnupg to enable verification.
# - xz to unpack downloads from install-tl.
# - gcc & musl-dev so config.guess detects musl instead of glibc.
#   Required as of config-guess commit 4aa554d72b2aad99506875401f798339c0df7cec
#   https://git.savannah.gnu.org/gitweb/?p=config.git;a=commitdiff;h=4aa554d72b2aad99506875401f798339c0df7cec
# packages potentially needed at runtime:
# - ghostscript, to convert eps to pdf (includegraphics for example)
RUN apk add --no-cache perl wget gnupg xz gcc musl-dev && \
    apk add --no-cache ghostscript

# CTAN mirror address
# Default is https://mirror.ctan.org/systems/texlive/tlnet
ARG CTAN_MIRROR="https://mirror.ctan.org/systems/texlive/tlnet"

# download and extract installer
RUN set -eu; \
    mkdir /installer; \
    wget -q -O - "$CTAN_MIRROR/install-tl-unx.tar.gz" | tar -zx -C /installer

# copy profile
COPY ./texlive.profile /

# run installer
RUN set -eu; \
    cd /installer/*; \
    ./install-tl -profile /texlive.profile -repository "$CTAN_MIRROR"

# Remove the stuff we dont need anymore.
# Normally it would just adds a new layer, actually increasing the size.
# But with kaniko --single-snapshot we will get only one layer at the end,
# so this is not a problem
RUN set -eu; \
    rm -r /installer; \
    apk del wget gnupg xz gcc musl-dev
