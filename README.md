# docker-texlive-ci

A docker image primarily aimed at CI servers for building LaTeX
documents, as it's installed without the documentation tree.

Assembled by running the
[TeXLive netinstaller](https://www.tug.org/texlive/acquire-netinstall.html)
in an `alpine:latest` image.


## Tag Policy
Only the `latest` tag is stable, all other tags are "unsupported" and
may disappear at any time. 

- `latest` and `ref-master` are synonymous. They point to the latest image
  built by the `master` branch. This image is automatically rebuilt every month.
- `ref-$BRANCH_NAME` points to the latest image built from the branch.
- `commit-$COMMIT_SHA` points to the latest image built from the commit.
- `build-$COMMIT_SHA-$DATE` is the result of the specified commit at the
  specified time.
- `texlive-YEAR` tag the latest builds with the named TeXLive releases.
  Currently done manually, so there's not tag for the current release.
  TODO: Automatically set the `texlive-YEAR` tags.
- All tags except `latest` and `texlive-.*` automatically expire after 30 days.
