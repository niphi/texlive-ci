#!/bin/sh
# set -e  Exit immediately if a command exits with a non-zero status.
# set -u  Treat unset variables as an error when substituting.
set -eu

# Absolute path to $0's directory, resolving all symlinks in the path
script_dir="$(dirname "$(readlink -f "$0")")"
readonly script_dir
cd -- "$script_dir"

# Build the docker image, tagging as as docker-texlive-ci:latest
docker build -t docker-texlive-ci:latest .
